
//Section - JSON objects
// json - javascript object notation


//Array JS objects sample
const cities = [
	{City: "Valenzuela", Province: "Manila", Country: 'Philippines'},
	{City: "Meycauayan", Province: "Bulacan", Country: 'Philippines'},
	{City: "Tueguegarao", Province: "benguet", Country: 'Philippines'},
	];
console.log(cities);

// Array JSON objects sample
//Json is used for serializing different data types into bytes
// bytes is a unit of data that is composed of eight binary digits that is used to represent a character(letters,numbers,typographical symbol)
// serialization is the process of converting data into a series of bytes for easier transmission/transfer of information.

const cities2 = [
	{"City": "Valenzuela", "Province": "Manila", "Country": 'Philippines'},
	{"City": "Meycauayan", "Province": "Bulacan", "Country": 'Philippines'},
	{"City": "Tueguegarao", "Province": "benguet", "Country": 'Philippines'},
	];
console.log(cities);
	
	
//Json methods

let batchesArr = [
	{batchName: "Batch X"},
	{batchName: "Batch Y"}
]
console.log(batchesArr);
// stringify method
// json object contains methods for parsing and converting data into/from json or stringified json
console.log("Result from stringify method");
console.log(JSON.stringify(batchesArr));

//mini activity

const data = `[
	{"name": "John Cena"},
	{"age": 45},
	{"Address":{city: "New york", country: "USA"}}
]`;
console.log(JSON.stringify(data));

//JSON.stringify with prompt()

let fname = prompt("What is your first name?");
let lname = prompt("What is your last name?");
let age = prompt("What is your age?");
let address = {
	city: prompt("Which city do you live?"),
	country: prompt("Which country does your city belong"),
};

let otherData = JSON.stringify({
	fname: fname,
	lname: lname,
	age: age,
	address: address
});
console.log(otherData);

//Section - parse

let batchesJSON = `[{"batchname": "Batch X"},{"batchname": "Batch Y"}]`;
console.log(batchesJSON);
console.log("Result from parse method");
console.log(JSON.parse(batchesJSON));

console.log(JSON.parse(otherData));